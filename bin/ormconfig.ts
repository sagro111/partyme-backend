import { DataSource } from 'typeorm';

import { databaseConfig } from '@/config/database';

const AppDataSource = new DataSource({
  ...databaseConfig,
  entities: [__dirname + '/../src/models/**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/../src/database/**/*-migration{.ts,.js}'],
});

export default AppDataSource;
