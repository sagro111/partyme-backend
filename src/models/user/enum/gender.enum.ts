export enum GenderEnum {
  Male = 'male',
  Female = 'female',
  Neutral = 'neutral',
}
