export enum EventInvitePossibilityEnum {
  All = 'all',
  Followers = 'followers',
  Followings = 'followings',
  NoBody = 'noBody',
}
