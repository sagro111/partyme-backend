export enum RoleEnum {
  PartyGoer = 'partyGoer',
  Guard = 'guard',
  PartyMaker = 'partyMaker',
}
