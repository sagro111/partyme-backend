export enum StatusEnum {
  'NotActive' = 'notActive',
  'Banned' = 'banned',
  'Active' = 'active',
}
