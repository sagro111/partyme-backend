import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { UserEntity } from './user.entity';
import { IUser } from '../interface/user.interface';

@Entity('followers')
export class FollowerEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => UserEntity, (u) => u.followings, {
    cascade: true,
  })
  follower: IUser;

  @ManyToOne(() => UserEntity, (u) => u.followers, {
    cascade: true,
  })
  following: IUser;
}
