import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'avatar' })
export class AvatarEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  url: string;
}
