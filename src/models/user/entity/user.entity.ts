import {
  AfterLoad,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
} from 'typeorm';

import dataSource from '@/bin/ormconfig';
import { BaseEntity } from '@/common/entity/base.entity';
import { hashPassword } from '@/common/utils';
import { RoleEnum } from '@/models/user/enum/role.enum';
import { StatusEnum } from '@/models/user/enum/status.enum';
import { GenderEnum } from '@/models/user/enum/gender.enum';
import { AddressEntity } from '@/models/address/entity/address.entity';
import { IAvatar } from '@/models/user/interface/avatar.interface';
import { IAddress } from '@/models/address/interface/address.interface';
import { IEvent } from '@/models/events/interface/event.interface';
import { EventEntity } from '@/models/events/entity/event.entity';
import { EventInvitePossibilityEnum } from '@/models/user/enum/event-invite-possibility.enum';
import { IFollower } from '@/models/user/interface/follower.interface';

import { FollowerEntity } from './follower.entity';
import { AvatarEntity } from './avatar.entity';

@Entity({ name: 'user' })
export class UserEntity extends BaseEntity {
  @Column({ type: 'text', unique: true })
  username: string;

  @Column({ nullable: true, length: 128 })
  password: string;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    if (this.password) this.password = await hashPassword(this.password);
  }

  @Column({ type: 'text' })
  firstName: string;

  @Column({ type: 'text' })
  lastName: string;

  @Column({ type: 'text', unique: true })
  phoneNumber: string;

  @Column({ type: 'text', nullable: true })
  email: string;

  @Column({ enum: GenderEnum })
  gender: GenderEnum;

  @Column({ enum: StatusEnum, default: StatusEnum.NotActive })
  status: StatusEnum;

  @Column({ enum: RoleEnum, default: RoleEnum.PartyGoer })
  role: RoleEnum;

  @OneToOne(() => AddressEntity, {
    cascade: true,
  })
  @JoinColumn()
  address: IAddress;

  @OneToOne(() => AvatarEntity, {
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  avatar: IAvatar;

  @ManyToMany(() => EventEntity)
  @JoinTable()
  joinedEvents: IEvent[];

  @OneToMany(() => EventEntity, (events) => events.owner)
  createdEvents: IEvent[];

  @ManyToMany(() => EventEntity, (events) => events.id)
  visitedEvents: IEvent[];

  @OneToMany(() => FollowerEntity, (f) => f.following, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  followers: IFollower[];
  followersCount: number;

  @OneToMany(() => FollowerEntity, (f) => f.follower, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  followings: IFollower[];
  followingsCount: number;

  @Column({
    enum: EventInvitePossibilityEnum,
    default: EventInvitePossibilityEnum.All,
  })
  invitePossibility: EventInvitePossibilityEnum;

  @AfterLoad()
  async count(): Promise<void> {
    const repository = dataSource.getRepository(FollowerEntity);

    const [, followingsCount] = await repository
      .createQueryBuilder('follow')
      .where('follow.followerId = :id', {
        id: this.id,
      })
      .getManyAndCount();

    const [, followersCount] = await repository
      .createQueryBuilder('follow')
      .where('follow.followingId = :id', {
        id: this.id,
      })
      .getManyAndCount();

    this.followersCount = followersCount;
    this.followingsCount = followingsCount;
  }
}
