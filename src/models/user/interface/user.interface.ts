import { RoleEnum } from '../enum/role.enum';
import { IAvatar } from './avatar.interface';
import { StatusEnum } from '../enum/status.enum';
import { IAddress } from '../../address/interface/address.interface';
import { IFollower } from './follower.interface';
import { IEvent } from '../../events/interface/event.interface';
import { EventInvitePossibilityEnum } from '../enum/event-invite-possibility.enum';

export type IUser = {
  id: string;
  username: string;
  avatar: IAvatar;
  status: StatusEnum;
  address: IAddress;
  role: RoleEnum;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  password: string;
  email: string;

  followers: IFollower[];
  followings: IFollower[];

  followersCount: number;
  followingsCount: number;

  joinedEvents: IEvent[];
  createdEvents: IEvent[];
  visitedEvents: IEvent[];

  invitePossibility: EventInvitePossibilityEnum;
};

export type IUserResponse = Omit<IUser, 'password'>;
