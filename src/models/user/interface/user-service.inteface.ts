import { FindOptionsWhere, Repository } from 'typeorm';

import { IUser } from './user.interface';

export interface IUserService {
  createUser: (data: Partial<IUser>) => Promise<IUser>;
  deleteUser: (id: string) => Promise<{ id: string }>;
  editUser: (data: Partial<IUser>) => Promise<IUser>;
  findOne: (field: FindOptionsWhere<IUser>) => Promise<IUser>;
  findMany: (
    where: FindOptionsWhere<IUser> | FindOptionsWhere<IUser>[],
  ) => Promise<IUser[]>;
  repository: Repository<IUser>;
}
