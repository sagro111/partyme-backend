import { Request } from 'express';

import { IUser } from './user.interface';

export type IExpressRequest = Omit<Request, 'user'> & {
  user: IUser | null;
};
