import { IUser } from './user.interface';

export interface IFollower {
  id: string;

  follower: IUser;

  following: IUser;
}
