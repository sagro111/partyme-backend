import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

import { IAvatar } from '../interface/avatar.interface';
import { IAddress } from '../../address/interface/address.interface';

export class UpdateUserDto {
  @IsString()
  @IsOptional()
  @IsNotEmpty()
  username: string;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  firstName: string;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  lastName: string;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  phoneNumber: string;

  @ApiModelProperty()
  @IsEmail()
  email: string;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  address: IAddress;

  @ApiModelProperty()
  @IsString()
  @IsOptional()
  avatar: IAvatar;
}
