import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

import { IAvatar } from '../interface/avatar.interface';
import { IAddress } from '../../address/interface/address.interface';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  username: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  phoneNumber: string;

  @ApiModelProperty()
  @IsEmail()
  email: string;

  @ApiModelProperty()
  @IsString()
  @IsString()
  address: IAddress;

  @ApiModelProperty()
  @IsString()
  avatar: IAvatar;
}
