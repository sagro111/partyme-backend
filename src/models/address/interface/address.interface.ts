export type ICityCoordinate = [lat: number, lng: number];
export interface IAddress {
  id: string;
  cityCoordinate: ICityCoordinate;
  city: string;
  country: string;
}
