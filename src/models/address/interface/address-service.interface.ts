import { IAddress } from './address.interface';

export interface IAddressService {
  add: () => Promise<IAddress>;
  edit: () => Promise<IAddress>;
  get: (id: string) => Promise<IAddress>;
}
