import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import {
  IAddress,
  ICityCoordinate,
} from '@/models/address/interface/address.interface';

@Entity({ name: 'address' })
export class AddressEntity implements IAddress {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text', { array: true, default: [] })
  cityCoordinate: ICityCoordinate;

  @Column({ type: 'text', default: null })
  city: string;

  @Column({ type: 'text', default: null })
  country: string;
}
