import { IsArray, IsNotEmpty, IsString } from 'class-validator';

import { ICityCoordinate } from '@/models/address/interface/address.interface';

export class AddressDto {
  @IsString()
  @IsArray()
  cityCoordinate: ICityCoordinate;

  @IsString()
  @IsNotEmpty()
  city: string;

  @IsString()
  @IsNotEmpty()
  country: string;
}
