import { IUser } from '@/models/user/interface/user.interface';
import { IEvent } from '@/models/events/interface/event.interface';

export interface IVisitorService {
  joinEvent: (userId: string, eventId: string) => Promise<IEvent>;
  leaveEvent: (userId: string, eventId: string) => Promise<void>;
  invite: (invitedUserId: string, eventId: string) => Promise<void>;

  visitorList: (eventId: string) => Promise<IUser[]>;
}
