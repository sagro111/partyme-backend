import { StatusEnum } from '@/models/user/enum/status.enum';
import { RoleEnum } from '@/models/user/enum/role.enum';

export interface IAccessTokenPayload {
  username: string;
  id: string;
  email: string;
  status: StatusEnum;
  role: RoleEnum;
}

export interface IRefreshTokenPayload {
  id: string;
}
