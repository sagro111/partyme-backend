import { JwtVerifyOptions } from '@nestjs/jwt';

import {
  IAccessTokenPayload,
  IRefreshTokenPayload,
} from './token-payload.inteface';
import { IToken } from './token.interface';

export interface ITokenService {
  createJwtToken: (
    payload: IAccessTokenPayload | IRefreshTokenPayload,
  ) => Promise<IToken>;
  verifyToken: (token: string, options?: JwtVerifyOptions) => Promise<void>;
  validateToken: (
    token: string,
  ) => Promise<IAccessTokenPayload | IRefreshTokenPayload>;
  decode: (token: string) => IAccessTokenPayload;
  isExist: (token: string) => Promise<boolean>;
}
