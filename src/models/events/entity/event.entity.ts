import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import dataSource from '@/bin/ormconfig';
import { BaseEntity } from '@/common/entity/base.entity';
import { GenderEnum } from '@/models/user/enum/gender.enum';
import { UserEntity } from '@/models/user/entity/user.entity';
import { IUser } from '@/models/user/interface/user.interface';

import { EventMetaInfoEntity } from './event-meta-info.entity';
import { EventPhotoEntity } from './event-photo.entity';
import { IAddress } from '../../address/interface/address.interface';
import { AddressEntity } from '../../address/entity/address.entity';
import { IEventPhoto } from '../interface/events-photo.interface';
import { IEventMetaInfo } from '../interface/events-meta.interface';

@Entity('events')
export class EventEntity extends BaseEntity {
  @Column({ type: 'text', nullable: true })
  ticketPrice: string | null;

  @Column({ nullable: true })
  genre: string;

  @Column({ type: 'boolean', default: false })
  generateQrCode: boolean;

  @Column({ type: 'boolean', default: false })
  isFaceControlled: boolean;

  @Column({ type: 'boolean', default: false })
  isPrivate: boolean;

  @Column({ type: 'boolean', default: false })
  isHiddenVisitor: boolean;

  @Column({ type: 'text', nullable: true })
  thematic: string;

  @Column({ enum: GenderEnum, nullable: true })
  onlyGender: GenderEnum;

  @ManyToOne(() => UserEntity, (user) => user.createdEvents)
  owner: IUser;

  @OneToOne(() => AddressEntity)
  @JoinColumn()
  address: IAddress;

  @OneToOne(() => EventMetaInfoEntity, {
    eager: true,
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  metaInfo: IEventMetaInfo;

  @ManyToMany(() => UserEntity)
  @JoinTable()
  visitors: IUser[];

  visitorsCount: number;

  @OneToMany(() => EventPhotoEntity, (photo) => photo.event)
  @JoinColumn()
  photos: IEventPhoto[];

  @AfterLoad()
  async count() {
    const [, count] = await dataSource.getRepository(UserEntity).findAndCount({
      where: {
        joinedEvents: {
          id: this.id,
        },
      },
    });

    this.visitorsCount = count;
  }
}
