import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { EventEntity } from './event.entity';
import { IEvent } from '../interface/event.interface';

@Entity('photo')
export class EventPhotoEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  url: string;

  @ManyToOne(() => EventEntity, (e) => e.photos)
  event: IEvent;
}
