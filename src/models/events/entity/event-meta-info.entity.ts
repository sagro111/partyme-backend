import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('event-meta')
export class EventMetaInfoEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text' })
  title: string;

  @Column({ type: 'text' })
  description: string;

  @Column({ type: 'date' })
  startDate: Date;
}
