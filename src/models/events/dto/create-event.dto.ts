import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateEventDto {
  @IsOptional()
  @IsNumberString()
  ticketPrice: string;

  @IsString()
  @MinLength(8)
  @IsNotEmpty()
  title: string;

  @IsString()
  @MinLength(40)
  @MaxLength(320)
  @IsNotEmpty()
  description: string;

  @IsDateString()
  @IsNotEmpty()
  startDate: Date;

  @IsOptional()
  @IsDateString()
  endDate: Date;

  @IsOptional()
  @IsNumber()
  minAge: number;
}
