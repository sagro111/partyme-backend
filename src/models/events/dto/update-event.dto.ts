import {
  IsBoolean,
  IsDate,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

import { AddressDto } from '@/models/address/dto/address.dto';
import { IEventPhoto } from '@/models/events/interface/events-photo.interface';
import { IAddress } from '@/models/address/interface/address.interface';
import { IUser } from '@/models/user/interface/user.interface';

export class UpdateEventDto {
  @IsString()
  @IsNotEmpty()
  id: string;

  @IsOptional()
  photos: IEventPhoto[];

  @IsOptional()
  @ValidateNested({ each: true, always: true })
  @Type(() => AddressDto)
  address: IAddress;

  @IsOptional()
  @ValidateNested({ each: true, always: true })
  visitors: IUser[];

  @IsOptional()
  @IsBoolean()
  isPrivate: boolean;

  @IsOptional()
  @IsString()
  genre: string;

  @IsOptional()
  @IsBoolean()
  isFaceControlled: boolean;

  @IsOptional()
  @IsBoolean()
  isHiddenVisitor: boolean;

  @IsOptional()
  @IsBoolean()
  generateQrCode: boolean;

  @IsOptional()
  @IsString()
  thematic: string;

  @IsOptional()
  @IsNumberString()
  @IsString()
  ticketPrice: string;

  @IsString()
  @MinLength(8)
  @IsNotEmpty()
  title: string;

  @IsString()
  @MinLength(40)
  @MaxLength(320)
  @IsNotEmpty()
  description: string;

  @IsISO8601()
  @IsNotEmpty()
  startDate: string;

  @IsOptional()
  @IsDate()
  endDate: Date;

  @IsOptional()
  @IsNumber()
  minAge: number;
}
