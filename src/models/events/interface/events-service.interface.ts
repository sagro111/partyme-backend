import { IEvent } from './event.interface';
import { ICreateEvent } from './create-event.interface';
import { IUpdateEvent } from './update-event.interface';

export interface IEventService {
  create: (userId: string, eventData: ICreateEvent) => Promise<IEvent>;
  edit: (userId: string, eventData: IUpdateEvent) => Promise<IEvent>;
  delete: (userId: string, eventId: string) => Promise<string>;
  list: () => Promise<IEvent[]>;
  getOne: (eventId: string) => Promise<IEvent>;
}
