export interface IEventPhoto {
  id: string;
  url: string;
}
