export interface ICreateEvent {
  ticketPrice: string;
  title: string;
  description: string;
  startDate: Date;
  endDate: Date;
  minAge: number;
}
