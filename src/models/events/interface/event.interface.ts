import { IUser } from '@/models/user/interface/user.interface';

import { IEventMetaInfo } from './events-meta.interface';
import { IEventPhoto } from './events-photo.interface';

export type ITicketPrice = string | null;

export interface IEvent {
  id: string;
  owner: IUser;
  visitors: IUser[];
  photos: IEventPhoto[];
  ticketPrice: ITicketPrice;
  isPrivate: boolean;
  genre: string;
  isFaceControlled: boolean;
  thematic: string;
  metaInfo: IEventMetaInfo;
}
