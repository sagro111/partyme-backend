export interface IEventMetaInfo {
  id: string;
  createAt: Date;
  updateAt: Date;
  title: string;
  description: string;
  startDate: Date;
  endDate: Date;
  minAge: number;
}
