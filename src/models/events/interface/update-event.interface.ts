import { IUser } from '@/models/user/interface/user.interface';

import { IEventPhoto } from './events-photo.interface';
import { IAddress } from '../../address/interface/address.interface';

export interface IUpdateEvent {
  id: string;

  photos: IEventPhoto[];

  address: IAddress;

  visitors: IUser[];

  isPrivate: boolean;

  genre: string;

  isFaceControlled: boolean;

  thematic: string;

  ticketPrice: string;

  title: string;

  description: string;

  startDate: string;

  endDate: Date;

  minAge: number;
}
