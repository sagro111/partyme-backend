import { RoleEnum } from '@/models/user/enum/role.enum';
import { IAvatar } from '@/models/user/interface/avatar.interface';

import { IAddress } from '../../address/interface/address.interface';

export type IProfile = {
  id: string;
  username: string;
  address: IAddress;
  avatar: IAvatar;
  role: RoleEnum;
  firstName: string;
  lastName: string;
  followersCount: number;
  followingsCount: number;

  isFollowing: boolean;
};
