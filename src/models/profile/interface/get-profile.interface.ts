export interface IGetProfile {
  currentUserId: string;
  username: string;
}
