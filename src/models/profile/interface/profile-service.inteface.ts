import { IUser } from '@/models/user/interface/user.interface';

import { IProfile } from './profile.interface';
import { IFollow } from './follow.interface';
import { IGetProfile } from './get-profile.interface';

export interface IProfileService {
  getProfile(data: IGetProfile): Promise<IProfile>;
  editProfile(data: Partial<IUser>): Promise<IProfile>;

  follow(data: IFollow): Promise<IProfile>;
  unfollow(data: IFollow): Promise<IProfile>;
}
