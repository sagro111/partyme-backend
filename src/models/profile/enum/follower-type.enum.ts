export enum FollowerTypeEnum {
  Followers = 'followers',
  Following = 'following',
}
