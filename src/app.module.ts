import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { DatabaseModule } from '@/providers/database/database.module';

import { UserModule } from './shared/user/user.module';
import { AuthModule } from './auth/auth.module';
import { ProfileModule } from './shared/profile/profile.module';
import { TokenModule } from './shared/token/token.module';
import { AddressModule } from './shared/address/address.module';
import { AuthMiddleware } from './auth/auth.middleware';
import { SmsModule } from './services/sms/sms.module';
import { EmailModule } from './services/email/email.module';
import { EventsModule } from './shared/events/events.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    }),
    DatabaseModule,
    UserModule,
    AuthModule,
    EventsModule,
    ProfileModule,
    SmsModule,
    EmailModule,
    TokenModule,
    AddressModule,
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes({
      method: RequestMethod.ALL,
      path: '*',
    });
  }
}
