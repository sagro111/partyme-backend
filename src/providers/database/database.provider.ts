import { Provider } from '@nestjs/common';

import { DATABASE_SOURCE_TOKEN } from '@/providers/database/database.constant';

import AppDataSource from '../../../bin/ormconfig';

export const DatabaseProvider: Provider = {
  provide: DATABASE_SOURCE_TOKEN,
  useFactory: async () => {
    return AppDataSource.initialize();
  },
};
