import { Module } from '@nestjs/common';

import { DATABASE_SOURCE_TOKEN, DatabaseProvider } from './';

@Module({
  providers: [DatabaseProvider],
  exports: [DatabaseProvider, DATABASE_SOURCE_TOKEN],
})
export class DatabaseModule {}
