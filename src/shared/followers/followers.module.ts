import { Module } from '@nestjs/common';

import { DatabaseModule } from '@/providers/database/database.module';

import { FollowersController } from './followers.controller';
import { FollowersService } from './followers.service';

@Module({
  imports: [DatabaseModule],
  controllers: [FollowersController],
  providers: [FollowersService],
})
export class FollowersModule {}
