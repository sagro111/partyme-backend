import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { JwtService, JwtVerifyOptions } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { JWT_TOKEN_REPOSITORY_TOKEN } from '@/models/token/token.constant';

import { ITokenService } from '../../models/token/interface/token-service.inteface';
import { TokenEntity } from '../../models/token/entity/token.entity';
import {
  IAccessTokenPayload,
  IRefreshTokenPayload,
} from '../../models/token/interface/token-payload.inteface';
import { IToken } from '../../models/token/interface/token.interface';

@Injectable()
export class TokenService implements ITokenService {
  private readonly accessTokenTtl: string;
  private readonly refreshTokenTtl: string;

  constructor(
    @Inject(JWT_TOKEN_REPOSITORY_TOKEN)
    private readonly tokenRepository: Repository<TokenEntity>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {
    this.accessTokenTtl = configService.getOrThrow<string>(
      'ACCESS_TOKEN_TIME_SEC',
    );
    this.refreshTokenTtl = configService.getOrThrow<string>(
      'REFRESH_TOKEN_TIME_SEC',
    );
  }

  private generateToken(
    payload: IAccessTokenPayload | IRefreshTokenPayload,
    ttl: string | number,
  ): string {
    return this.jwtService.sign(payload, { expiresIn: `${ttl}s` });
  }

  async createJwtToken(payload: IAccessTokenPayload): Promise<IToken> {
    const accessToken = this.generateToken(payload, this.accessTokenTtl);
    const refreshToken = this.generateToken(
      { id: payload.id },
      this.refreshTokenTtl,
    );

    const token: IToken = {
      accessToken,
      refreshToken,
    };

    await this.tokenRepository.save(token);
    return { accessToken, refreshToken };
  }

  decode(token: string): IAccessTokenPayload {
    return this.jwtService.decode(token) as IAccessTokenPayload;
  }

  async verifyToken(token: string, options?: JwtVerifyOptions): Promise<void> {
    try {
      await this.jwtService.verify(token, options);
    } catch (e) {
      throw new HttpException(
        {
          message: 'Your token is invalid.',
          statusMessage: 'INVALID_TOKEN',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  async validateToken(
    token: string,
  ): Promise<IAccessTokenPayload | IRefreshTokenPayload> {
    try {
      await this.verifyToken(token);
      await this.isExist(token);
      return this.decode(token);
    } catch (e) {
      throw new UnauthorizedException({
        message: e.message,
        status: 'ACCESS_TOKEN_IS_EXPIRE',
      });
    }
  }

  async isExist(token: string): Promise<boolean> {
    const isExist = this.tokenRepository.findOneBy({
      accessToken: token,
    });

    if (!isExist) {
      throw new UnauthorizedException('We can not find this token');
    }
    return Boolean(isExist);
  }
}
