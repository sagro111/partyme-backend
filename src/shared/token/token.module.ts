import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { JWT_TOKEN_SERVICE_TOKEN } from '@/models/token/token.constant';
import { DatabaseModule } from '@/providers/database/database.module';

import { TokenProviders } from './token.provider';
import { JwtStrategy } from '../../auth/strategy/jwt.strategy';

const jwtFactory = {
  useFactory: async (configService: ConfigService) => ({
    secret: configService.get('JWT_SECRET'),
    signOptions: {
      expiresIn: '60m',
    },
  }),
  inject: [ConfigService],
};

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync(jwtFactory),
    ConfigModule,
    DatabaseModule,
  ],
  providers: TokenProviders,
  exports: [JWT_TOKEN_SERVICE_TOKEN, JwtModule, JwtStrategy, PassportModule],
})
export class TokenModule {}
