import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';

import {
  JWT_TOKEN_REPOSITORY_TOKEN,
  JWT_TOKEN_SERVICE_TOKEN,
} from '@/models/token/token.constant';
import { TokenEntity } from '@/models/token/entity/token.entity';
import { JwtStrategy } from '@/auth/strategy/jwt.strategy';
import { JwtRefreshStrategy } from '@/auth/strategy/jwt-refresh.strategy';
import { DATABASE_SOURCE_TOKEN } from '@/providers/database';

import { TokenService } from './token.service';

export const TokenProviders: Provider[] = [
  JwtStrategy,
  JwtRefreshStrategy,
  {
    provide: JWT_TOKEN_SERVICE_TOKEN,
    useClass: TokenService,
  },
  {
    provide: JWT_TOKEN_REPOSITORY_TOKEN,
    inject: [DATABASE_SOURCE_TOKEN],
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TokenEntity),
  },
];
