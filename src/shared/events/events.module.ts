import { Module } from '@nestjs/common';
import { RouterModule } from 'nest-router';

import { VisitorsModule } from './visitors/visitors.module';
import { EventModule } from './event/event.module';

@Module({
  imports: [
    RouterModule.forRoutes([
      {
        path: '/events',
        module: EventModule,
        children: [
          {
            path: '/:eventId/visitors',
            module: VisitorsModule,
          },
        ],
      },
    ]),
    EventModule,
    VisitorsModule,
  ],
  exports: [EventModule, VisitorsModule],
})
export class EventsModule {}
