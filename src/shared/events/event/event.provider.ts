import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';

import {
  EVENT_REPOSITORY_TOKEN,
  EVENT_SERVICE_TOKEN,
} from '@/models/events/event.constant';
import { EventEntity } from '@/models/events/entity/event.entity';
import { DATABASE_SOURCE_TOKEN } from '@/providers/database';

import { EventService } from './event.service';

export const EventProvider: Provider[] = [
  {
    provide: EVENT_SERVICE_TOKEN,
    useClass: EventService,
  },
  {
    provide: EVENT_REPOSITORY_TOKEN,
    inject: [DATABASE_SOURCE_TOKEN],
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(EventEntity),
  },
];
