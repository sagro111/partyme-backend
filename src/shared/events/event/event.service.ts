import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Repository } from 'typeorm';

import { EVENT_REPOSITORY_TOKEN } from '@/models/events/event.constant';
import { USER_REPOSITORY_TOKEN } from '@/models/user/user.constant';

import { IEventService } from '../../../models/events/interface/events-service.interface';
import { IEvent } from '../../../models/events/interface/event.interface';
import { IUpdateEvent } from '../../../models/events/interface/update-event.interface';
import { ICreateEvent } from '../../../models/events/interface/create-event.interface';
import { EventEntity } from '../../../models/events/entity/event.entity';
import { UserEntity } from '../../../models/user/entity/user.entity';
import { RoleEnum } from '../../../models/user/enum/role.enum';
import { IEventMetaInfo } from '../../../models/events/interface/events-meta.interface';

@Injectable()
export class EventService implements IEventService {
  constructor(
    @Inject(EVENT_REPOSITORY_TOKEN)
    private readonly eventsRepository: Repository<EventEntity>,
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async create(userId: string, eventData: ICreateEvent): Promise<IEvent> {
    const owner = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['createdEvents'],
    });
    if (owner === null) {
      throw new BadRequestException('This user does not exist');
    }
    if (owner.role !== RoleEnum.PartyMaker) {
      throw new UnauthorizedException(
        "You don't have permission to create event",
      );
    }

    const metaInfo: Partial<IEventMetaInfo> = {
      startDate: eventData.startDate,
      description: eventData.description,
      title: eventData.title,
    };

    const event = this.eventsRepository.create({
      ...eventData,
      metaInfo,
      owner,
    });

    await this.eventsRepository.save(event);

    return await this.eventsRepository.findOneOrFail({
      where: { id: event.id },
      relations: ['owner'],
    });
  }

  async delete(userId: string, eventId: string): Promise<string> {
    const event = await this.checkEventsOwner(userId, eventId);

    if (event.owner.id !== userId) {
      throw new UnauthorizedException(
        "You don't have permission to delete this event",
      );
    }

    await this.eventsRepository.softDelete(eventId);
    return event.id;
  }

  async edit(userId: string, eventData: IUpdateEvent): Promise<IEvent> {
    const event = await this.checkEventsOwner(userId, eventData.id);

    return this.eventsRepository.save({ ...event, ...eventData });
  }

  async getOne(eventId: string): Promise<IEvent> {
    try {
      return await this.eventsRepository.findOneOrFail({
        where: {
          id: eventId,
        },
        relations: ['visitors', 'photos', 'address', 'owner'],
      });
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  async list(): Promise<IEvent[]> {
    return this.eventsRepository.find({ relations: ['owner'] });
  }

  private async checkEventsOwner(
    userId: string,
    eventId: string,
  ): Promise<IEvent> {
    const event = await this.eventsRepository.findOne({
      where: {
        id: eventId,
      },
      relations: ['owner'],
    });

    if (event === null) {
      throw new NotFoundException('This event does not exist');
    }

    if (event.owner.id !== userId) {
      throw new BadRequestException(
        "You don't have permission to edit this event",
      );
    }

    return event;
  }
}
