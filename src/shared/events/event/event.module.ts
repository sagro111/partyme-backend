import { Module } from '@nestjs/common';

import { EVENT_REPOSITORY_TOKEN } from '@/models/events/event.constant';
import { DatabaseModule } from '@/providers/database/database.module';

import { EventController } from './event.controller';
import { EventProvider } from './event.provider';
import { AddressModule } from '../../address/address.module';
import { UserModule } from '../../user/user.module';

@Module({
  imports: [AddressModule, UserModule, DatabaseModule],
  controllers: [EventController],
  providers: EventProvider,
  exports: [EVENT_REPOSITORY_TOKEN],
})
export class EventModule {}
