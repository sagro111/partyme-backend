import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';

import { EVENT_SERVICE_TOKEN } from '@/models/events/event.constant';

import { IEventService } from '../../../models/events/interface/events-service.interface';
import { JwtAuthGuard } from '../../../common/guards/auth.guard';
import { User } from '../../user/user.decorator';
import { CreateEventDto } from '../../../models/events/dto/create-event.dto';
import { UpdateEventDto } from '../../../models/events/dto/update-event.dto';
import { IResponse } from '../../../common/interceptor/transform-response';
import { IEvent } from '../../../models/events/interface/event.interface';

@Controller()
export class EventController {
  constructor(
    @Inject(EVENT_SERVICE_TOKEN) private readonly eventService: IEventService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @User('id') userId: string,
    @Body() dto: CreateEventDto,
  ): Promise<IResponse<IEvent>> {
    const data = await this.eventService.create(userId, dto);
    return { message: 'Success fetched', data };
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':eventId')
  async delete(
    @User('id') userId: string,
    @Param('eventId') eventId: string,
  ): Promise<IResponse<string>> {
    const data = await this.eventService.delete(userId, eventId);
    return { message: 'Success delete', data };
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async edit(
    @User('id') userId: string,
    @Body() dto: UpdateEventDto,
  ): Promise<IResponse<IEvent>> {
    const data = await this.eventService.edit(userId, dto);
    return { message: 'Success fetched', data };
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async list(): Promise<IResponse<IEvent[]>> {
    const data = await this.eventService.list();
    return { message: 'Success fetched', data };
  }

  @UseGuards(JwtAuthGuard)
  @Get(':eventId')
  async getOne(@Param('eventId') eventId: string): Promise<IResponse<IEvent>> {
    const data = await this.eventService.getOne(eventId);
    return { message: 'Success fetched', data };
  }
}
