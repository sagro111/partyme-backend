import { Provider } from '@nestjs/common';

import { VISITOR_SERVICE_TOKEN } from '@/models/visitors/visitors.constant';

import { VisitorsService } from './visitors.service';

export const VisitorsProviders: Provider[] = [
  {
    provide: VISITOR_SERVICE_TOKEN,
    useClass: VisitorsService,
  },
];
