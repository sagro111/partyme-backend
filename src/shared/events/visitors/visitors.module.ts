import { Module } from '@nestjs/common';
import { UserModule } from 'src/shared/user/user.module';

import { DatabaseModule } from '@/providers/database/database.module';

import { VisitorsController } from './visitors.controller';
import { EventModule } from '../event/event.module';
import { VisitorsProviders } from './visitors.providers';

@Module({
  imports: [UserModule, DatabaseModule, EventModule],
  controllers: [VisitorsController],
  providers: VisitorsProviders,
})
export class VisitorsModule {}
