import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Repository } from 'typeorm';

import { isFreeEvent } from '@/shared/events/event/utils';
import { USER_REPOSITORY_TOKEN } from '@/models/user/user.constant';
import { EVENT_REPOSITORY_TOKEN } from '@/models/events/event.constant';
import { IVisitorService } from '@/models/visitors/interface/visitors-service.interface';
import { UserEntity } from '@/models/user/entity/user.entity';
import { EventEntity } from '@/models/events/entity/event.entity';
import { IEvent } from '@/models/events/interface/event.interface';
import { IUser } from '@/models/user/interface/user.interface';

@Injectable()
export class VisitorsService implements IVisitorService {
  constructor(
    @Inject(EVENT_REPOSITORY_TOKEN)
    private readonly eventsRepository: Repository<EventEntity>,
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async visitorList(eventId: string): Promise<IUser[]> {
    const event = await this.findEvent(eventId);

    return event.visitors;
  }

  async invite(userId: string, eventId: string): Promise<void> {
    const { event, visitor } = await this.checkUserForJoin(eventId, userId);
  }

  async joinEvent(userId: string, eventId: string): Promise<IEvent> {
    const { event, visitor } = await this.checkUserForJoin(eventId, userId);

    if (isFreeEvent(event.ticketPrice)) {
      visitor.joinedEvents.push(event);
      event.visitors.push(visitor);

      await this.userRepository.save(visitor);
      await this.eventsRepository.save(event);
    }

    return event;
  }

  async leaveEvent(userId: string, eventId: string): Promise<void> {
    const event = await this.findEvent(eventId);
    event.visitors = event.visitors.filter(({ id }) => id !== userId);
    await this.eventsRepository.save(event);
  }

  private async checkUserForJoin(eventId: string, userId: string) {
    const event = await this.findEvent(eventId);
    const visitor = await this.findVisitor(userId);

    if (event.owner.id === visitor.id) {
      throw new BadRequestException(
        'Owner can not join or leave from this event',
      );
    }

    const alreadyInvited = await this.eventsRepository.findOneBy({
      visitors: { id: visitor.id },
    });

    if (alreadyInvited !== null) {
      throw new BadRequestException('This user already joined');
    }

    return { event, visitor };
  }

  private async findVisitor(userId: string): Promise<IUser> {
    const visitor = await this.userRepository.findOne({
      where: {
        id: userId,
      },
      relations: ['joinedEvents'],
    });

    if (visitor === null) {
      throw new NotFoundException('This visitor does not exist');
    }

    return visitor;
  }

  private async findEvent(eventId: string): Promise<IEvent> {
    const event = await this.eventsRepository.findOne({
      where: {
        id: eventId,
      },
      relations: ['visitors', 'owner'],
    });

    if (event === null) {
      throw new NotFoundException('This event does not exist');
    }

    return event;
  }
}
