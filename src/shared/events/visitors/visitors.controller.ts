import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';

import { VISITOR_SERVICE_TOKEN } from '@/models/visitors/visitors.constant';

import { IVisitorService } from '../../../models/visitors/interface/visitors-service.interface';
import { JwtAuthGuard } from '../../../common/guards/auth.guard';
import { User } from '../../user/user.decorator';
import { IResponse } from '../../../common/interceptor/transform-response';
import { IUser } from '../../../models/user/interface/user.interface';

@Controller()
export class VisitorsController {
  constructor(
    @Inject(VISITOR_SERVICE_TOKEN)
    private readonly visitorService: IVisitorService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async joinEvent(
    @User('id') userId: string,
    @Param('eventId') eventId: string,
  ): Promise<IResponse<never>> {
    await this.visitorService.joinEvent(userId, eventId);
    return { message: 'You have been successfully joined' };
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  async leaveEvent(
    @User('id') userId: string,
    @Param('eventId') eventId: string,
  ): Promise<IResponse<never>> {
    await this.visitorService.leaveEvent(userId, eventId);
    return { message: 'You have been successfully leave' };
  }

  @UseGuards(JwtAuthGuard)
  @Post('invite')
  async invite(
    @Param('eventId') eventId: string,
    @Body('inviterUserId') invitedUserId: string,
  ): Promise<IResponse<never>> {
    await this.visitorService.invite(invitedUserId, eventId);
    return { message: 'Your friend has been invited' };
  }

  @Get()
  async visitorsList(
    @Param('eventId') eventId: string,
  ): Promise<IResponse<IUser[]>> {
    const data = await this.visitorService.visitorList(eventId);
    return { message: 'You have been successfully leave', data };
  }
}
