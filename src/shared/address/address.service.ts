import { Injectable } from '@nestjs/common';

import { IAddressService } from '../../models/address/interface/address-service.interface';
import { IAddress } from '../../models/address/interface/address.interface';

@Injectable()
export class AddressService implements IAddressService {
  async add(): Promise<IAddress> {
    return {} as IAddress;
  }

  async edit(): Promise<IAddress> {
    return {} as IAddress;
  }

  async get(id: string): Promise<IAddress> {
    return {} as IAddress;
  }
}
