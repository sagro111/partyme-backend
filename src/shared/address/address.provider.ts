import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';

import { AddressEntity } from '@/models/address/entity/address.entity';
import { DATABASE_SOURCE_TOKEN } from '@/providers/database';

import {
  ADDRESS_REPOSITORY_TOKEN,
  ADDRESS_SERVICE_TOKEN,
} from './address.constant';
import { AddressService } from './address.service';

export const AddressProvider: Provider[] = [
  {
    provide: ADDRESS_SERVICE_TOKEN,
    useClass: AddressService,
  },
  {
    provide: ADDRESS_REPOSITORY_TOKEN,
    inject: [DATABASE_SOURCE_TOKEN],
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(AddressEntity),
  },
];
