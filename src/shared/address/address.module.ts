import { Module } from '@nestjs/common';

import { DatabaseModule } from '@/providers/database/database.module';

import { AddressController } from './address.controller';
import { AddressProvider } from './address.provider';
import {
  ADDRESS_REPOSITORY_TOKEN,
  ADDRESS_SERVICE_TOKEN,
} from './address.constant';

@Module({
  imports: [DatabaseModule],
  controllers: [AddressController],
  providers: AddressProvider,
  exports: [ADDRESS_SERVICE_TOKEN, ADDRESS_REPOSITORY_TOKEN],
})
export class AddressModule {}
