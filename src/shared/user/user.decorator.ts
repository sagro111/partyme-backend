import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { IExpressRequest } from '../../models/user/interface/express-request.inteface';
import { IUser } from '../../models/user/interface/user.interface';

export const User = createParamDecorator(
  (data: keyof IUser, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest<IExpressRequest>();

    if (req.user === null) {
      return null;
    }
    if (data) {
      return req.user[data];
    }
    return req.user;
  },
);
