import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';

import {
  FOLLOWERS_REPOSITORY_TOKEN,
  USER_REPOSITORY_TOKEN,
  USER_SERVICE_TOKEN,
} from '@/models/user/user.constant';
import { UserEntity } from '@/models/user/entity/user.entity';
import { FollowerEntity } from '@/models/user/entity/follower.entity';
import { DATABASE_SOURCE_TOKEN } from '@/providers/database';

import { UserService } from './user.service';

export const UserProviders: Provider[] = [
  {
    provide: USER_SERVICE_TOKEN,
    useClass: UserService,
  },
  {
    provide: USER_REPOSITORY_TOKEN,
    inject: [DATABASE_SOURCE_TOKEN],
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(UserEntity),
  },
  {
    provide: FOLLOWERS_REPOSITORY_TOKEN,
    inject: [DATABASE_SOURCE_TOKEN],
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(FollowerEntity),
  },
];
