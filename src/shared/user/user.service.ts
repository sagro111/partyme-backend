import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { FindOptionsWhere, Repository } from 'typeorm';

import { USER_REPOSITORY_TOKEN } from '@/models/user/user.constant';

import { IUserService } from '../../models/user/interface/user-service.inteface';
import { IUser } from '../../models/user/interface/user.interface';
import { IAddress } from '../../models/address/interface/address.interface';
import { ADDRESS_REPOSITORY_TOKEN } from '../address/address.constant';
import { IAvatar } from '../../models/user/interface/avatar.interface';

@Injectable()
export class UserService implements IUserService {
  constructor(
    @Inject(ADDRESS_REPOSITORY_TOKEN)
    private readonly addressRepository: Repository<IAddress>,
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<IUser>,
  ) {}

  async createUser(data: Partial<IUser>): Promise<IUser> {
    try {
      const { avatar, address, ...user } = data;
      const createdUser = this.userRepository.create(user);

      if (avatar !== undefined) {
        createdUser.avatar = await this.addAvatar(avatar);
      }

      if (address !== undefined) {
        createdUser.address = await this.addAddress(address);
      }

      return this.userRepository.save(createdUser);
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async deleteUser(id: string): Promise<{ id: string }> {
    try {
      await this.userRepository.delete({ id });
      return { id };
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async editUser(body: Partial<IUser>): Promise<IUser> {
    try {
      const user = await this.userRepository.findOneByOrFail({
        username: body.username,
      });
      this.userRepository.merge(user, body);

      return await this.userRepository.save(user);
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  async findMany(
    where: FindOptionsWhere<IUser> | FindOptionsWhere<IUser>[],
  ): Promise<IUser[]> {
    try {
      return this.userRepository.find({ where });
    } catch (e) {
      throw new Error(e.message);
    }
  }

  private async addAddress(address: IAddress): Promise<IAddress> {
    try {
      const userAddress = this.addressRepository.create(address);
      await this.addressRepository.save(userAddress);

      return userAddress;
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }

  private async addAvatar(avatar: IAvatar): Promise<IAvatar> {
    // try {
    //   const userAvatar = this.addressRepository.create({});
    //   await this.addressRepository.save(userAvatar);
    //
    //   return userAvatar as unknown as IAvatar;
    // } catch (e) {
    //   throw new NotFoundException(e.message);
    // }
    return 0 as unknown as IAvatar;
  }

  get repository(): Repository<IUser> {
    return this.userRepository;
  }

  async findOne(field: FindOptionsWhere<IUser>): Promise<IUser> {
    try {
      return await this.userRepository.findOneByOrFail(field);
    } catch (e) {
      throw new NotFoundException(e.message);
    }
  }
}
