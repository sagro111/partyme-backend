import { Module } from '@nestjs/common';

import {
  FOLLOWERS_REPOSITORY_TOKEN,
  USER_REPOSITORY_TOKEN,
  USER_SERVICE_TOKEN,
} from '@/models/user/user.constant';
import { DatabaseModule } from '@/providers/database/database.module';

import { UserController } from './user.controller';
import { UserProviders } from './user.provider';
import { AddressModule } from '../address/address.module';

@Module({
  imports: [DatabaseModule, AddressModule],
  controllers: [UserController],
  providers: UserProviders,
  exports: [
    USER_REPOSITORY_TOKEN,
    FOLLOWERS_REPOSITORY_TOKEN,
    USER_SERVICE_TOKEN,
  ],
})
export class UserModule {}
