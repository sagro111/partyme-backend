import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import { USER_SERVICE_TOKEN } from '@/models/user/user.constant';

import { IUserService } from '../../models/user/interface/user-service.inteface';
import { CreateUserDto } from '../../models/user/dto/create-user.dto';
import { UpdateUserDto } from '../../models/user/dto/edit-user.dto';

@Controller('user')
export class UserController {
  constructor(
    @Inject(USER_SERVICE_TOKEN) private readonly userService: IUserService,
  ) {}

  @Post()
  async createUser(@Body() dto: CreateUserDto) {
    return this.userService.createUser(dto);
  }

  @Get(':username')
  async getUser(@Param('username') username: string) {
    return this.userService.findOne({ username });
  }

  @Put()
  async editUser(@Body() dto: UpdateUserDto) {
    return this.userService.editUser(dto);
  }

  @Delete(':username')
  async deleteUser(@Param('username') username: string) {
    return this.userService.deleteUser(username);
  }
}
