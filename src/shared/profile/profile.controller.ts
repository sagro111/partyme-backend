import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';

import { PROFILE_SERVICE_TOKEN } from '@/models/profile/profile.constant';

import { IProfileService } from '../../models/profile/interface/profile-service.inteface';
import { IProfile } from '../../models/profile/interface/profile.interface';
import { IResponse } from '../../common/interceptor/transform-response';
import { FollowDto } from '../../models/profile/dto/follow.dto';
import { User } from '../user/user.decorator';
import { JwtAuthGuard } from '../../common/guards/auth.guard';

@Controller('profile')
export class ProfileController {
  constructor(
    @Inject(PROFILE_SERVICE_TOKEN)
    private readonly profileService: IProfileService,
  ) {}

  @Get(':username')
  @UseGuards(JwtAuthGuard)
  async gerProfile(
    @User('id') currentUserId: string,
    @Param('username') username: string,
  ): Promise<IResponse<IProfile>> {
    const data = await this.profileService.getProfile({
      currentUserId,
      username,
    });
    return { message: 'User fetched successfully.', data };
  }

  @Post('follow')
  @UseGuards(JwtAuthGuard)
  async follow(
    @User('id') followerId: string,
    @Body() { followingId }: FollowDto,
  ): Promise<IResponse<IProfile>> {
    const data = await this.profileService.follow({
      followerId,
      followingId,
    });
    return { message: 'User fetched successfully.', data };
  }

  @Post('unfollow')
  @UseGuards(JwtAuthGuard)
  async unfollow(
    @User('id') followerId: string,
    @Body() { followingId }: FollowDto,
  ): Promise<IResponse<IProfile>> {
    const data = await this.profileService.unfollow({
      followerId,
      followingId,
    });
    return { message: 'User fetched successfully.', data };
  }
}
