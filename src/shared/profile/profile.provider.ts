import { Provider } from '@nestjs/common';

import { PROFILE_SERVICE_TOKEN } from '@/models/profile/profile.constant';

import { ProfileService } from './profile.service';

export const ProfileProvider: Provider[] = [
  {
    provide: PROFILE_SERVICE_TOKEN,
    useClass: ProfileService,
  },
];
