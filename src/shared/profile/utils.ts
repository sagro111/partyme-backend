import { UserEntity } from '../../models/user/entity/user.entity';
import { IProfile } from '../../models/profile/interface/profile.interface';

export const userToUserProfile = (user: UserEntity): IProfile => ({
  id: user.id,
  username: user.username,
  avatar: user.avatar,
  role: user.role,
  firstName: user.firstName,
  lastName: user.lastName,
  followersCount: user.followersCount || 0,
  followingsCount: user.followingsCount || 0,
  isFollowing: false,
  address: user.address,
});
