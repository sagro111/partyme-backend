import { Module } from '@nestjs/common';

import { PROFILE_SERVICE_TOKEN } from '@/models/profile/profile.constant';

import { UserModule } from '../user/user.module';
import { ProfileController } from './profile.controller';
import { ProfileProvider } from './profile.provider';

@Module({
  imports: [UserModule],
  providers: ProfileProvider,
  controllers: [ProfileController],
  exports: [PROFILE_SERVICE_TOKEN],
})
export class ProfileModule {}
