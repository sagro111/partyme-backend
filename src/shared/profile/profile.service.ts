import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Repository } from 'typeorm';

import {
  FOLLOWERS_REPOSITORY_TOKEN,
  USER_REPOSITORY_TOKEN,
} from '@/models/user/user.constant';

import { IProfileService } from '../../models/profile/interface/profile-service.inteface';
import { IProfile } from '../../models/profile/interface/profile.interface';
import { IUser } from '../../models/user/interface/user.interface';
import { userToUserProfile } from './utils';
import { FollowerEntity } from '../../models/user/entity/follower.entity';
import { IFollow } from '../../models/profile/interface/follow.interface';
import { IGetProfile } from '../../models/profile/interface/get-profile.interface';
import { UserEntity } from '../../models/user/entity/user.entity';

@Injectable()
export class ProfileService implements IProfileService {
  constructor(
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<UserEntity>,

    @Inject(FOLLOWERS_REPOSITORY_TOKEN)
    private readonly followRepository: Repository<FollowerEntity>,
  ) {}

  async getProfile({
    currentUserId,
    username,
  }: IGetProfile): Promise<IProfile> {
    const user = await this.userRepository.findOne({
      where: { username },
      relations: ['address', 'avatar'],
    });

    if (user === null) {
      throw new NotFoundException('This user does not exist.');
    }

    const followingEntity = await this.getUserFollowedEntity({
      followerId: currentUserId,
      followingId: user.id,
    });

    const profile = userToUserProfile(user);
    return { ...profile, isFollowing: Boolean(followingEntity) };
  }

  async editProfile(data: Partial<IUser>): Promise<IProfile> {
    throw new Error('Method not implemented.');
  }

  async follow({ followerId, followingId }: IFollow): Promise<IProfile> {
    const { follower, following } = await this.getFollowerAndFollowing({
      followerId,
      followingId,
    });

    const followingEntity = await this.getUserFollowedEntity({
      followerId,
      followingId,
    });

    if (followingEntity === null) {
      await this.followRepository.save({
        follower,
        following,
      });
    }

    const profile = userToUserProfile(following);
    return { ...profile, isFollowing: true };
  }

  async unfollow({ followerId, followingId }: IFollow): Promise<IProfile> {
    const user = await this.userRepository.findOneByOrFail({ id: followerId });
    const following = await this.getUserFollowedEntity({
      followerId,
      followingId,
    });

    if (following) {
      await this.followRepository.delete(following.id);
    }

    return userToUserProfile(user);
  }
  async getUserFollowedEntity({
    followerId,
    followingId,
  }: IFollow): Promise<FollowerEntity | null> {
    return this.followRepository.findOne({
      where: {
        follower: { id: followerId },
        following: { id: followingId },
      },
    });
  }

  private async getFollowerAndFollowing({ followerId, followingId }) {
    const follower = await this.userRepository.findOne({
      where: { id: followerId },
      relations: ['followers', 'followings'],
    });
    const following = await this.userRepository.findOne({
      where: { id: followingId },
      relations: ['followers', 'followings'],
    });

    if (follower === null || following === null) {
      throw new NotFoundException('following user is not defined');
    }

    if (follower.id === following.id) {
      throw new BadRequestException('Follower and following can not be equal.');
    }
    return {
      follower,
      following,
    };
  }
}
