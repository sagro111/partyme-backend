import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';

import { AppModule } from './app.module';
import { TransformCatch } from './common/interceptor/transform-catch';
import { TransformResponseInterceptor } from './common/interceptor/transform-response';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalInterceptors(new TransformResponseInterceptor());
  app.useGlobalFilters(new TransformCatch());
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());

  await app.listen(3000);
}

bootstrap();
