type ObjectType = Record<string, unknown>;

export const isObject = <T>(value: T): value is T & ObjectType =>
  value !== null && typeof value === 'object' && !Array.isArray(value);

export const merge = <T extends ObjectType, U extends ObjectType>(
  target: T,
  source: U,
): T & U => {
  for (const key of Object.keys(source)) {
    const targetValue = target[key];
    const sourceValue = source[key];
    if (isObject(targetValue) && isObject(sourceValue)) {
      Object.assign(sourceValue, merge(targetValue, sourceValue));
    }
  }

  return { ...target, ...source };
};
