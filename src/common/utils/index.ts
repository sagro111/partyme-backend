import { genSalt, hash } from 'bcrypt';
import parsePhoneNumberFromString from 'libphonenumber-js';

export const hashPassword = async (password: string): Promise<string> => {
  const salt = await genSalt(5);
  return await hash(password, salt);
};

export const isPhoneNumber = (phoneNumber: string): boolean => {
  const parsedPhone = parsePhoneNumberFromString(phoneNumber);

  return parsedPhone?.isValid() ?? false;
};
