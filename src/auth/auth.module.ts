import { Module } from '@nestjs/common';

import { UserModule } from '@/shared/user/user.module';
import { TokenModule } from '@/shared/token/token.module';
import { DatabaseModule } from '@/providers/database/database.module';
import { EmailModule } from '@/services/email/email.module';
import { SmsModule } from '@/services/sms/sms.module';

import { AuthController } from './auth.controller';
import { AUTH_SERVICE_TOKEN } from './auth.constant';
import { AuthProviders } from './auth.provider';

@Module({
  imports: [DatabaseModule, UserModule, SmsModule, EmailModule, TokenModule],
  controllers: [AuthController],
  providers: AuthProviders,
  exports: [AUTH_SERVICE_TOKEN],
})
export class AuthModule {}
