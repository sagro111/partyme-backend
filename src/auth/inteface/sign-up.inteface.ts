import { GenderEnum } from '@/models/user/enum/gender.enum';
import { IAvatar } from '@/models/user/interface/avatar.interface';
import { IAddress } from '@/models/address/interface/address.interface';

export interface ISignUp {
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  phoneNumber: string;

  address: IAddress;
  avatar: IAvatar;
  gender: GenderEnum;
}
