export interface IConfirmPhoneNumber {
  code: string;
  username: string;
}
