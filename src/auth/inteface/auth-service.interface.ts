import { IUserResponse } from '@/models/user/interface/user.interface';
import { IToken } from '@/models/token/interface/token.interface';

import { ISignUp } from './sign-up.inteface';
import { ISignIn } from './sign-in.interface';
import { IConfirmPhoneNumber } from './confirm-phone-number.interface';
import { IResetPassword } from './reset-password';
import { IConfirmResetPassword } from './confirm-reset-password.interface';

export interface IAuthService {
  signIn: (data: ISignIn) => Promise<IToken>;
  signUp: (data: ISignUp) => Promise<IUserResponse>;
  refresh: (userId: string) => Promise<IToken>;
  confirmPhoneNumber: (data: IConfirmPhoneNumber) => Promise<IToken>;
  resetPassword: (data: IResetPassword) => Promise<void>;
  confirmResetPassword: (data: IConfirmResetPassword) => Promise<IToken>;
}
