export interface IConfirmResetPassword {
  username: string;
  password: string;
  confirmPassword: string;
  code: string;
}
