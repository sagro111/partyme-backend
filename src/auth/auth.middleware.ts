import { Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { Repository } from 'typeorm';

import { IExpressRequest } from '@/models/user/interface/express-request.inteface';
import { USER_REPOSITORY_TOKEN } from '@/models/user/user.constant';
import { IUser } from '@/models/user/interface/user.interface';
import { JWT_TOKEN_SERVICE_TOKEN } from '@/models/token/token.constant';
import { ITokenService } from '@/models/token/interface/token-service.inteface';
import { IToken } from '@/models/token/interface/token.interface';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<IUser>,

    @Inject(JWT_TOKEN_SERVICE_TOKEN)
    private readonly tokenService: ITokenService,
  ) {}

  async use(req: IExpressRequest, _: Response, next: NextFunction) {
    const cookie: IToken | undefined = req.cookies['authorization'];
    if (cookie === undefined) {
      req.user = null;
      next();
      return;
    }
    try {
      const { id } = this.tokenService.decode(cookie.accessToken);
      req.user = await this.userRepository.findOneByOrFail({ id });
      next();
    } catch (e) {
      req.user = null;
      next();
    }
  }
}
