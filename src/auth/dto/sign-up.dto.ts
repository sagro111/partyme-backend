import {
  IsEnum,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  MinLength,
} from 'class-validator';

import { IAvatar } from '@/models/user/interface/avatar.interface';
import { GenderEnum } from '@/models/user/enum/gender.enum';
import { MatchWithField } from '@/common/decorators/match.decorator';
import { IAddress } from '@/models/address/interface/address.interface';

export class SignUpDto {
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsNotEmpty()
  @IsString()
  lastName: string;

  @IsNotEmpty()
  @IsPhoneNumber()
  phoneNumber: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  password: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MatchWithField('password', { message: 'Passwords must match' })
  confirmPassword: string;

  @IsEnum(GenderEnum)
  @IsNotEmpty()
  gender: GenderEnum;

  address: IAddress;
  avatar: IAvatar;
}
