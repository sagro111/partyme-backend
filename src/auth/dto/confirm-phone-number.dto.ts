import { IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';

export class ConfirmPhoneNumberDto {
  @IsNotEmpty()
  @IsString()
  @IsPhoneNumber()
  phoneNumber: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  code: string;
}
