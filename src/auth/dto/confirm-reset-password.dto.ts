import { IsNotEmpty, IsString, MinLength } from 'class-validator';

import { MatchWithField } from '@/common/decorators/match.decorator';

export class ConfirmResetPasswordDto {
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  password: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MatchWithField('password', { message: 'Passwords must match' })
  confirmPassword: string;

  @IsNotEmpty()
  @IsString()
  code: string;
}
