import { Provider } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AUTH_SERVICE_TOKEN } from './auth.constant';
import { JwtStrategy } from './strategy/jwt.strategy';

export const AuthProviders: Provider[] = [
  {
    provide: AUTH_SERVICE_TOKEN,
    useClass: AuthService,
  },
  JwtStrategy,
];
