import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';

import { JWT_TOKEN_SERVICE_TOKEN } from '@/models/token/token.constant';
import { ITokenService } from '@/models/token/interface/token-service.inteface';
import { IAccessTokenPayload } from '@/models/token/interface/token-payload.inteface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @Inject(JWT_TOKEN_SERVICE_TOKEN)
    private readonly tokenService: ITokenService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req) => {
          const data = req.cookies['authorization'];
          if (!data) {
            return null;
          }
          return data.accessToken;
        },
      ]),
      passReqToCallback: true,
      ignoreExpiration: true,
      secretOrKey: configService.get('JWT_SECRET'),
    });
  }

  async validate(req: Request, payload: IAccessTokenPayload) {
    const token = req.cookies['authorization'].accessToken;

    if (token === undefined) {
      throw new UnauthorizedException('Please authorize');
    }
    try {
      await this.tokenService.validateToken(token);
      return payload;
    } catch (e) {
      throw new HttpException(
        {
          message: e.message,
          statusMessage: 'ACCESS_TOKEN_IS_EXPIRE',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
