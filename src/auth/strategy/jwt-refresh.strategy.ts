import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';

import { JWT_TOKEN_SERVICE_TOKEN } from '@/models/token/token.constant';
import { ITokenService } from '@/models/token/interface/token-service.inteface';
import { IAccessTokenPayload } from '@/models/token/interface/token-payload.inteface';

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor(
    @Inject(JWT_TOKEN_SERVICE_TOKEN)
    private readonly tokenService: ITokenService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req) => {
          const data = req.cookies['authorization'];
          if (!data) {
            return null;
          }
          return data.accessToken;
        },
      ]),
      ignoreExpiration: true,
      secretOrKey: configService.get('JWT_SECRET'),
      passReqToCallback: true,
    });
  }

  async validate(req: Request, payload: IAccessTokenPayload) {
    const token = req.cookies['authorization'].refreshToken;

    if (token === undefined) {
      throw new UnauthorizedException('Please authorize');
    }
    try {
      await this.tokenService.validateToken(token);
      return payload;
    } catch (e) {
      throw new HttpException(
        {
          message: e.message,
          statusMessage: 'REFRESH_TOKEN_IS_EXPIRE',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
