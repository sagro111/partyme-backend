import { Body, Controller, Inject, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';

import { IResponse } from '@/common/interceptor/transform-response';
import { IUserResponse } from '@/models/user/interface/user.interface';
import { IToken } from '@/models/token/interface/token.interface';
import { JwtRefreshGuard } from '@/common/guards/refresh.guard';
import { User } from '@/shared/user/user.decorator';

import { AUTH_SERVICE_TOKEN } from './auth.constant';
import { IAuthService } from './inteface/auth-service.interface';
import { SignUpDto } from './dto/sign-up.dto';
import { SignInDto } from './dto/sign-in.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ConfirmPhoneNumberDto } from './dto/confirm-phone-number.dto';
import { ConfirmResetPasswordDto } from './dto/confirm-reset-password.dto';

@Controller('auth')
export class AuthController {
  constructor(
    @Inject(AUTH_SERVICE_TOKEN) private readonly authService: IAuthService,
  ) {}

  @Post('sign-up')
  async signUp(@Body() dto: SignUpDto): Promise<IResponse<IUserResponse>> {
    const data = await this.authService.signUp(dto);
    return { message: 'User successfully signed up.', data };
  }

  @Post('sign-in')
  async signIn(
    @Body() dto: SignInDto,
    @Res({ passthrough: true }) res: Response,
  ): Promise<IResponse<unknown>> {
    const data = await this.authService.signIn(dto);
    res.cookie('authorization', data, { httpOnly: true });
    return { message: 'Sign in successfully.' };
  }

  @Post('refresh')
  @UseGuards(JwtRefreshGuard)
  async refresh(
    @User('id') userId: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<IResponse<IToken>> {
    const data = await this.authService.refresh(userId);
    res.cookie('authorization', data, { httpOnly: true });
    return { message: 'Token successfully refreshed.' };
  }

  @Post('confirm-phone')
  async confirmPhone(
    @Body() dto: ConfirmPhoneNumberDto,
  ): Promise<IResponse<IToken>> {
    const data = await this.authService.confirmPhoneNumber(dto);
    return { message: 'Your phone number was successfully confirmed.', data };
  }

  @Post('reset-pass')
  async resetPass(@Body() dto: ResetPasswordDto): Promise<IResponse<null>> {
    await this.authService.resetPassword(dto);
    return {
      message: 'Your password successfully reset.',
      data: null,
    };
  }

  @Post('confirm-pass')
  async confirmPass(
    @Body() dto: ConfirmResetPasswordDto,
  ): Promise<IResponse<IToken>> {
    const data = await this.authService.confirmResetPassword(dto);
    return {
      message: 'Your password was successfully changed.',
      data,
    };
  }
}
