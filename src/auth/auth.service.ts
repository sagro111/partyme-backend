import {
  BadRequestException,
  Inject,
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { compare } from 'bcrypt';
import { Repository } from 'typeorm';

import {
  USER_REPOSITORY_TOKEN,
  USER_SERVICE_TOKEN,
} from '@/models/user/user.constant';
import { StatusEnum } from '@/models/user/enum/status.enum';
import { IUserResponse } from '@/models/user/interface/user.interface';
import { JWT_TOKEN_SERVICE_TOKEN } from '@/models/token/token.constant';
import { ITokenService } from '@/models/token/interface/token-service.inteface';
import { IToken } from '@/models/token/interface/token.interface';
import { IAccessTokenPayload } from '@/models/token/interface/token-payload.inteface';
import { IUserService } from '@/models/user/interface/user-service.inteface';
import { UserEntity } from '@/models/user/entity/user.entity';
import { IAuthService } from '@/auth/inteface/auth-service.interface';
import { IEmailService } from '@/services/email/interface/email-service.interface';
import { ISmsService } from '@/services/sms/inteface/sms-service.interface';
import { EMAIL_SERVICE_TOKEN } from '@/services/email/email.provider';
import { SMS_SERVICE_TOKEN } from '@/services/sms/sms.constant';
import { ISignIn } from '@/auth/inteface/sign-in.interface';
import { ISignUp } from '@/auth/inteface/sign-up.inteface';
import { IConfirmResetPassword } from '@/auth/inteface/confirm-reset-password.interface';
import { hashPassword } from '@/common/utils';
import { IConfirmPhoneNumber } from '@/auth/inteface/confirm-phone-number.interface';
import { IResetPassword } from '@/auth/inteface/reset-password';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    @Inject(USER_REPOSITORY_TOKEN)
    private readonly userRepository: Repository<UserEntity>,

    @Inject(USER_SERVICE_TOKEN) private readonly userService: IUserService,
    @Inject(EMAIL_SERVICE_TOKEN) private readonly emailService: IEmailService,
    @Inject(SMS_SERVICE_TOKEN) private readonly smsService: ISmsService,
    @Inject(JWT_TOKEN_SERVICE_TOKEN)
    private readonly tokenService: ITokenService,
  ) {}

  async signIn({ username, password }: ISignIn): Promise<IToken> {
    const user = await this.validateUser(password, username);

    if (user.status === StatusEnum.NotActive) {
      throw new UnauthorizedException('Your account is not active');
    }

    return this.authorizeUser(user);
  }

  async signUp(data: ISignUp): Promise<IUserResponse> {
    const alreadyRegisteredUser = await this.findRegisteredUser(
      data.username,
      data.phoneNumber,
    );

    if (alreadyRegisteredUser !== null) {
      throw new UnauthorizedException('This user already has');
    }

    try {
      const user = await this.userService.createUser({
        ...data,
        status: StatusEnum.NotActive,
      });

      // await this.smsService.sendVerificationMessage(user.phoneNumber);
      const { password, ...responseUser } = user;
      return responseUser;
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  private async findRegisteredUser(username: string, phoneNumber: string) {
    return this.userService.repository
      .createQueryBuilder('user')
      .where('user.username = :username OR user.phoneNumber = :phoneNumber', {
        username,
        phoneNumber,
      })
      .getOne();
  }

  async resetPassword({ username }: IResetPassword): Promise<void> {
    const user = await this.userService.findOne({ username });
    if (user === null) {
      throw new UnprocessableEntityException('Can not find user');
    }
    try {
      await this.smsService.sendVerificationMessage(user.phoneNumber);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  async confirmResetPassword(data: IConfirmResetPassword): Promise<IToken> {
    const { username, password, confirmPassword, code } = data;

    const user = await this.userService.findOne({ username });

    const hashedPassword = await hashPassword(password);
    const newUser = await this.userService.editUser({
      ...user,
      status: StatusEnum.Active,
      password: hashedPassword,
    });

    return this.authorizeUser(newUser);
  }

  async confirmPhoneNumber({
    code,
    username,
  }: IConfirmPhoneNumber): Promise<IToken> {
    const registeredUser = await this.userService.findOne({ username });
    if (!registeredUser) {
      throw new UnauthorizedException('Entity not found');
    }
    if (registeredUser.status === StatusEnum.Active) {
      throw new BadRequestException('Phone number already confirmed');
    }

    try {
      await this.smsService.confirmVerificationMessage(
        registeredUser.phoneNumber,
        code,
      );
      const user = await this.activateUser(username);
      return this.authorizeUser(user);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  private async activateUser(username: string): Promise<IUserResponse> {
    const user = this.userService.findOne({ username });
    return await this.userService.editUser({
      ...user,
      status: StatusEnum.Active,
    });
  }

  async refresh(userId: string): Promise<IToken> {
    const user = await this.userService.findOne({ id: userId });
    console.log(user);
    try {
      return this.authorizeUser(user);
    } catch (e) {
      throw new UnauthorizedException(e.message);
    }
  }

  private async authorizeUser(user: IUserResponse): Promise<IToken> {
    const tokenPayload: IAccessTokenPayload = {
      username: user.username,
      id: user.id,
      status: user.status,
      email: user.email,
      role: user.role,
    };
    return this.tokenService.createJwtToken(tokenPayload);
  }

  private async validateUser(
    password: string,
    username: string,
  ): Promise<IUserResponse> {
    const user = await this.userRepository.findOneBy({ username });
    if (user === null) {
      throw new UnprocessableEntityException('User not found');
    }
    const match = await compare(password, user.password);

    if (!match || !user) {
      throw new UnauthorizedException('Incorrect password or username');
    }

    if (user && match) {
      const { password, ...rest } = user;
      return rest;
    }
    throw new UnauthorizedException('Incorrect password or username');
  }
}
