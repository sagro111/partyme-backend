import { DataSourceOptions } from 'typeorm';

export const databaseConfig: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'igorlatyshev',
  password: 'Sol220365',
  database: 'partyme',
  synchronize: true,
  migrationsTableName: 'migrations',
};
