import * as Joi from 'joi';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { configuration } from './configuration';
import { TwilioConfigService } from './twilio.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        TWILIO_SID: Joi.string().default('twilio'),
        TWILIO_AUTH_TOKEN: Joi.string().default('twilio'),
        TWILIO_SERVICE_ID: Joi.string().default('twilio'),
      }),
    }),
  ],
  providers: [TwilioConfigService],
})
export class TwilioConfigModule {}
