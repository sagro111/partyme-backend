import { registerAs } from '@nestjs/config';

export const configuration = registerAs('twilio', () => ({
  twilioSid: process.env.TWILIO_SID,
  twilioAuthToken: process.env.TWILIO_AUTH_TOKEN,
  twilioServiceId: process.env.TWILIO_SERVICE_ID,
}));
