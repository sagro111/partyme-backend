import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TwilioConfigService {
  constructor(private readonly configService: ConfigService) {}

  get twilioSid(): string {
    return this.configService.getOrThrow('TWILIO_SID');
  }
  get twilioAuthToken(): string {
    return this.configService.getOrThrow('TWILIO_AUTH_TOKEN');
  }
  get twilioServiceId(): string {
    return this.configService.getOrThrow('TWILIO_SERVICE_ID');
  }
}
