import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';

import { configuration } from './configuration';
import { NodeMailerConfigService } from './node-mailer.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        EMAIL_SERVICE: Joi.string().default('email'),
        EMAIL_HOST: Joi.string().default('email'),
        EMAIL_USER: Joi.string().default('email'),
        EMAIL_PASSWORD: Joi.string().default('email'),
      }),
    }),
  ],
  providers: [ConfigService, NodeMailerConfigService],
  exports: [NodeMailerConfigService],
})
export class NodeMailerConfigModule {}
