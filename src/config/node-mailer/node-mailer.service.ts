import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class NodeMailerConfigService {
  constructor(private readonly configService: ConfigService) {}

  get emailService(): string {
    return this.configService.getOrThrow<string>('EMAIL_SERVICE');
  }
  get emailHost(): string {
    return this.configService.getOrThrow<string>('EMAIL_HOST');
  }
  get emailUser(): string {
    return this.configService.getOrThrow<string>('EMAIL_USER');
  }
  get emailPassword(): string {
    return this.configService.getOrThrow<string>('EMAIL_PASSWORD');
  }
}
