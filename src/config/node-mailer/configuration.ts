import { registerAs } from '@nestjs/config';

export const configuration = registerAs('node-mailer', () => ({
  emailService: process.env.EMAIL_SERVICE,
  emailHost: process.env.EMAIL_HOST,
  emailUser: process.env.EMAIL_USER,
  emailPassword: process.env.EMAIL_PASSWORD,
}));
