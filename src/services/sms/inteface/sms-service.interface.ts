export interface ISmsService {
  sendMessage: (phoneNumber: string, message: string) => void;
  sendVerificationMessage: (phoneNumber: string) => Promise<unknown>;
  confirmVerificationMessage: (
    phoneNumber: string,
    code: string,
  ) => Promise<boolean>;
}
