import { Provider } from '@nestjs/common';

import { SmsService } from './sms.service';
import { SMS_SERVICE_TOKEN } from './sms.constant';

export const SmsProviders: Provider[] = [
  {
    provide: SMS_SERVICE_TOKEN,
    useClass: SmsService,
  },
];
