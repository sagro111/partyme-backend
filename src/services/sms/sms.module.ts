import { Module } from '@nestjs/common';

import { TwilioConfigModule } from '@/config/twilio';

import { SmsController } from './sms.controller';
import { SmsProviders } from './sms.provide';
import { SMS_SERVICE_TOKEN } from './sms.constant';

@Module({
  imports: [TwilioConfigModule],
  controllers: [SmsController],
  providers: SmsProviders,
  exports: [SMS_SERVICE_TOKEN],
})
export class SmsModule {}
