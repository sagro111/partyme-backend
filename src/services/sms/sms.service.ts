import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Twilio } from 'twilio';
import { VerificationInstance } from 'twilio/lib/rest/verify/v2/service/verification';

import { ISmsService } from './inteface/sms-service.interface';

@Injectable()
export class SmsService implements ISmsService {
  private readonly twilioSid: string;
  private readonly twilioAuthToken: string;

  private readonly twilioClient: Twilio;

  constructor(private readonly configService: ConfigService) {
    this.twilioSid = configService.getOrThrow('TWILIO_SID');
    this.twilioAuthToken = configService.getOrThrow('TWILIO_AUTH_TOKEN');

    this.twilioClient = new Twilio(this.twilioSid, this.twilioAuthToken);
  }

  async confirmVerificationMessage(
    phoneNumber: string,
    code: string,
  ): Promise<boolean> {
    const twilioServiceId = this.configService.getOrThrow('TWILIO_SERVICE_ID');

    const result = await this.twilioClient.verify
      .services(twilioServiceId)
      .verificationChecks.create({ to: phoneNumber, code });

    if (!result.valid || result.status !== 'approved') {
      throw new BadRequestException('Wrong code provided');
    }
    return result.valid;
  }

  sendMessage(phoneNumber: string, message: string): void {}

  async sendVerificationMessage(
    phoneNumber: string,
  ): Promise<VerificationInstance> {
    const twilioServiceId = this.configService.getOrThrow('TWILIO_SERVICE_ID');
    return await this.twilioClient.verify
      .services(twilioServiceId)
      .verifications.create({
        to: phoneNumber,
        channel: 'sms',
        locale: 'en',
      });
  }
}
