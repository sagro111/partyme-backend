import { EmailTemplateTypeEnum } from '../enum/email-template-type.enum';

export interface SendMessage {
  emailTemplate: EmailTemplateTypeEnum;
  message: string;
  title: string;
  to: string;
}

export interface IEmailService {
  sendMessage: (data: SendMessage) => Promise<void>;
}
