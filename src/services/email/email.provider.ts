import { Provider } from '@nestjs/common';

import { EmailService } from './email.service';

export const EMAIL_SERVICE_TOKEN = 'EMAIL_SERVICE_TOKEN';
export const MAILER_SERVICE_TOKEN = 'MAILER_SERVICE_TOKEN';

export const EmailServiceProvider: Provider = {
  provide: EMAIL_SERVICE_TOKEN,
  useClass: EmailService,
};
