import { join } from 'path';
import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

import {
  NodeMailerConfigModule,
  NodeMailerConfigService,
} from '@/config/node-mailer';

import { EMAIL_SERVICE_TOKEN, EmailServiceProvider } from './email.provider';
import { EmailController } from './email.controller';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [NodeMailerConfigModule],
      inject: [NodeMailerConfigService],
      useFactory: (configService: NodeMailerConfigService) => ({
        transport: {
          service: configService.emailService,
          host: configService.emailHost,
          auth: {
            user: configService.emailUser,
            pass: configService.emailPassword,
          },
        },
        defaults: {
          from: configService.emailUser,
        },
        template: {
          dir: join(__dirname, './templates'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
  ],
  controllers: [EmailController],
  providers: [EmailServiceProvider],
  exports: [EMAIL_SERVICE_TOKEN],
})
export class EmailModule {}
