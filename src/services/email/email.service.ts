import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { SentMessageInfo } from 'nodemailer';

import {
  IEmailService,
  SendMessage,
} from './interface/email-service.interface';

@Injectable()
export class EmailService implements IEmailService {
  constructor(private readonly mailService: MailerService) {}

  sendMessage(data: SendMessage): Promise<SentMessageInfo> {
    return this.mailService.sendMail({
      to: data.to,
      text: data.message,
      template: `/${data.emailTemplate}`,
      context: {
        title: data.title,
      },
    });
  }
}
