import { Controller, Inject } from '@nestjs/common';

import { EMAIL_SERVICE_TOKEN } from './email.provider';
import { IEmailService } from './interface/email-service.interface';

@Controller('email')
export class EmailController {
  constructor(
    @Inject(EMAIL_SERVICE_TOKEN) private readonly emailService: IEmailService,
  ) {}
}
