export enum EmailTemplateTypeEnum {
  SignIn = 'signIn',
  SignUp = 'signUp',
  ResetPassword = 'resetPassword',
}
